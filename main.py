import sys
import numpy as np
from tabulate import tabulate


def convertir_a_lista(texto):
    return [int(num) for num in texto.split('\t')]  # suponemos que la el texto está separado por tabulares


def leer_matriz():
    print("Introduzca la matriz:")
    entrada = [convertir_a_lista(input())]  # matriz inicialmente con un renglón
    cant_renglones = 0
    cant_columnas = len(entrada[0]) - 1
    linea = input()
    while linea != 'listo':
        entrada.append(convertir_a_lista(linea))
        cant_renglones += 1
        linea = input()
    return np.array(entrada), cant_renglones, cant_columnas


def suma_producto(mat1, mat2, cant_ren, cant_col):
    suma = 0
    for i in range(cant_ren):
        for j in range(cant_col):
            suma += mat1[i, j] * mat2[i, j]
    return suma


def asignar_recursos(matriz, cant_renglones, cant_columnas, sol, i, j):
    actual = min(matriz[i, cant_columnas], matriz[cant_renglones, j])
    if actual == 0:
        return matriz
    sol[i, j] = actual
    matriz[i, cant_columnas] -= actual
    matriz[cant_renglones, j] -= actual
    matriz[cant_renglones, cant_columnas] -= actual
    return matriz


def imprimir_sol(matriz, cant_renglones, cant_columnas, sol):
    print("\nSolución:")
    print(tabulate(sol))
    print("Valor óptimo: " + str(suma_producto(matriz, sol, cant_renglones, cant_columnas)))


def esquina_noroeste():
    matriz, cant_renglones, cant_columnas = leer_matriz()
    sol = np.zeros((cant_renglones, cant_columnas))

    for ren in range(cant_renglones):
        for col in range(cant_columnas):
            asignar_recursos(matriz, cant_columnas, cant_renglones, sol, ren, col)

    imprimir_sol(matriz, cant_renglones, cant_columnas, sol)


def obtener_mayor_diferencia(matriz, cant_renglones, cant_columnas, renglones_primero, indice_est_global=None,
                             indice_rep_global=None, diferencia_mayor=-1):

    if renglones_primero:
        primero, segundo = cant_renglones, cant_columnas
    else:
        primero, segundo = cant_columnas, cant_renglones
        indice_est_global, indice_rep_global = indice_rep_global, indice_est_global

    for i in range(primero):
        if renglones_primero:
            if matriz[i, cant_columnas] == 0:
                continue
        elif matriz[cant_renglones, i] == 0:
            continue
        indice_rep_menor = None
        menor = sys.maxsize
        segundo_menor = sys.maxsize
        for j in range(segundo):
            if renglones_primero:
                if matriz[j, cant_columnas] == 0:
                    continue
                actual = matriz[i, j]
            else:
                if matriz[cant_renglones, j] == 0:
                    continue
                actual = matriz[j, i]
            if actual < menor:
                segundo_menor = menor
                indice_rep_menor = j
                menor = actual
            elif actual < segundo_menor:
                segundo_menor = actual
        if menor == sys.maxsize or segundo_menor == sys.maxsize:
            continue
        diferencia = segundo_menor - menor
        if diferencia > diferencia_mayor:
            diferencia_mayor = diferencia
            indice_est_global = i
            indice_rep_global = indice_rep_menor
    if renglones_primero:
        return indice_est_global, indice_rep_global, diferencia_mayor
    else:
        return indice_rep_global, indice_est_global, diferencia_mayor


def vogel():
    matriz, cant_renglones, cant_columnas = leer_matriz()
    sol = np.zeros((cant_renglones, cant_columnas))

    anterior = 0
    actual = matriz[cant_renglones, cant_columnas]
    while anterior != actual:
        indice_ren_global, indice_col_global, diferencia_mayor = obtener_mayor_diferencia(matriz, cant_renglones,
                                                                                          cant_columnas, True)
        indice_ren_global, indice_col_global, diferencia_mayor \
            = obtener_mayor_diferencia(matriz, cant_renglones, cant_columnas, False, indice_ren_global,
                                       indice_col_global, diferencia_mayor)
        anterior = matriz[cant_renglones, cant_columnas]
        matriz = asignar_recursos(matriz, cant_columnas, cant_renglones, sol, indice_ren_global, indice_col_global)
        actual = matriz[cant_renglones, cant_columnas]
    else:
        for i in range(cant_columnas):
            if matriz[cant_renglones, i] != 0:
                for j in range(cant_renglones):
                    if matriz[j, cant_columnas] != 0:
                        asignar_recursos(matriz, cant_renglones, cant_columnas, sol, j, i)
    imprimir_sol(matriz, cant_renglones, cant_columnas, sol)


met = input("Elija un método:\n1: Esquina noroeste\n2: Vogel\n")
if met == '1':
    esquina_noroeste()
elif met == '2':
    vogel()
